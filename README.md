# java-test-default
The goal was to implement simple rest api responsible for JavaScript frameworks management and add tests to new functionalities.


## Features 
* CRUD operations for JavaScript frameworks and search by name.

There is prepared endpoint located at `/api/v1/frameworks`.

## API Description
More detailed in javadoc <br />
`JavaScriptFramework`:
* GET `/api/v1/frameworks` gets all javascript frameworks
* GET `/api/v1/frameworks/{id}` gets javascript framework with the given id
* GET `/api/v1/frameworks/search?name=` gets javascript frameworks with the given name alias (beginning of the javascript framework name)
* POST `/api/v1/frameworks` creates new javascript framework
* PUT `/api/v1/frameworks/{id}` updates javascript framework with the given id
* DELETE `/api/v1/frameworks/{id}` deletes javascript framework with the given id

