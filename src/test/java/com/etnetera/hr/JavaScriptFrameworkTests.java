package com.etnetera.hr;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.etnetera.hr.data.HypeLevel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * Class used for Spring Boot/MVC based tests.
 *
 * @author Etnetera
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class JavaScriptFrameworkTests {

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper mapper = new ObjectMapper();

    private static final String BASE_ENDPOINT = "/api/v1/frameworks";

    @Autowired
    private JavaScriptFrameworkRepository repository;

    private void prepareData() {
        JavaScriptFramework react = new JavaScriptFramework("ReactJS", "1.1.1",
                LocalDate.of(2021, 12, 5), HypeLevel.LOW);
        JavaScriptFramework vue = new JavaScriptFramework("Vue.js", "1.1.1",
                LocalDate.of(2021, 12, 17), HypeLevel.MEDIUM);

        repository.save(react);
        repository.save(vue);
    }

    @Test
    public void frameworksTest() throws Exception {
        prepareData();

        mockMvc.perform(get(BASE_ENDPOINT)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("ReactJS")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("Vue.js")));
    }

    @Test
    public void addFrameworkInvalid() throws Exception {
        JavaScriptFramework framework = new JavaScriptFramework(null, "1.1.1", null, null);
        mockMvc.perform(post(BASE_ENDPOINT).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[0].field", is("name")))
                .andExpect(jsonPath("$.errors[0].message", is("NotEmpty")));

        framework.setName("verylongnameofthejavascriptframeworkjavaisthebest");
        mockMvc.perform(post(BASE_ENDPOINT).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[0].field", is("name")))
                .andExpect(jsonPath("$.errors[0].message", is("Size")));

    }

    @Test
    public void getAllJavaScriptFrameworksTest() throws Exception {
        List<JavaScriptFramework> frameworks = Arrays.asList(
                new JavaScriptFramework("Angular", "1.1.1", LocalDate.of(2021, 12, 22), HypeLevel.HIGH),
                new JavaScriptFramework("Vue.js", "2.1.1", LocalDate.of(2022, 1, 8), HypeLevel.LOW),
                new JavaScriptFramework("ReactJS", "3.1.1", LocalDate.of(2022, 4, 3), HypeLevel.MEDIUM)
        );
        repository.saveAll(frameworks);

        mockMvc.perform(get(BASE_ENDPOINT))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name", is(frameworks.get(0).getName())))
                .andExpect(jsonPath("[0].version", is(frameworks.get(0).getVersion())))
                .andExpect(jsonPath("[0].deprecationDate", is(frameworks.get(0).getDeprecationDate().toString())))
                .andExpect(jsonPath("[0].hypeLevel", is(frameworks.get(0).getHypeLevel().toString())))
                .andExpect(jsonPath("[1].name", is(frameworks.get(1).getName())))
                .andExpect(jsonPath("[1].version", is(frameworks.get(1).getVersion())))
                .andExpect(jsonPath("[1].deprecationDate", is(frameworks.get(1).getDeprecationDate().toString())))
                .andExpect(jsonPath("[1].hypeLevel", is(frameworks.get(1).getHypeLevel().toString())))
                .andExpect(jsonPath("[2].name", is(frameworks.get(2).getName())))
                .andExpect(jsonPath("[2].version", is(frameworks.get(2).getVersion())))
                .andExpect(jsonPath("[2].deprecationDate", is(frameworks.get(2).getDeprecationDate().toString())))
                .andExpect(jsonPath("[2].hypeLevel", is(frameworks.get(2).getHypeLevel().toString())));
    }

    @Test
    public void getJavaScriptFrameworkByIdTest() throws Exception {
        JavaScriptFramework framework = new JavaScriptFramework("Angular", "0.2.1", LocalDate.of(2021, 12, 22), HypeLevel.MEDIUM);
        repository.save(framework);
        String endpoint = BASE_ENDPOINT + "/" + framework.getId();

        mockMvc.perform(get(endpoint))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name", is(framework.getName())))
                .andExpect(jsonPath("$.version", is(framework.getVersion())))
                .andExpect(jsonPath("$.deprecationDate", is(framework.getDeprecationDate().toString())))
                .andExpect(jsonPath("$.hypeLevel", is(framework.getHypeLevel().toString())));
    }

    @Test
    public void searchJavaScriptFrameworksByStartingName() throws Exception {
        List<JavaScriptFramework> frameworks = Arrays.asList(
                new JavaScriptFramework("Angular", "1.1.1", LocalDate.of(2021, 12, 25), HypeLevel.HIGH),
                new JavaScriptFramework("Vue.js", "1.1.1", LocalDate.of(2021, 11, 22), HypeLevel.LOW),
                new JavaScriptFramework("AngularJS", "1.1.1", LocalDate.of(2021, 9, 12), HypeLevel.MEDIUM)
        );
        repository.saveAll(frameworks);

        String searchParam = "Ang";
        String endpoint = BASE_ENDPOINT + "/search?name=" + searchParam;

        mockMvc.perform(get(endpoint))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(frameworks.get(0).getName())))
                .andExpect(jsonPath("[1].name", is(frameworks.get(2).getName())));
    }

    @Test
    public void saveJavaScriptFrameworkTest() throws Exception {
        JavaScriptFramework framework = new JavaScriptFramework("Angular", "1.1.1", LocalDate.of(2021, 12, 22), HypeLevel.MEDIUM);

        mockMvc.perform(post(BASE_ENDPOINT).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(framework.getName())))
                .andExpect(jsonPath("$.version", is(framework.getVersion())))
                .andExpect(jsonPath("$.deprecationDate", is(framework.getDeprecationDate().toString())))
                .andExpect(jsonPath("$.hypeLevel", is(framework.getHypeLevel().toString())));
    }

    @Test
    public void saveJavaScriptFrameworkWithoutVersionTest() throws Exception {
        JavaScriptFramework framework = new JavaScriptFramework("Angular", null, LocalDate.of(2021, 12, 22), HypeLevel.MEDIUM);

        mockMvc.perform(post(BASE_ENDPOINT).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[0].field", is("version")))
                .andExpect(jsonPath("$.errors[0].message", is("NotEmpty")));
    }

    @Test
    public void saveJavaScriptFrameworkWithoutName() throws Exception {
        JavaScriptFramework framework = new JavaScriptFramework(null, "1.1.1", LocalDate.of(2021, 12, 22), HypeLevel.MEDIUM);

        mockMvc.perform(post(BASE_ENDPOINT).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[0].field", is("name")))
                .andExpect(jsonPath("$.errors[0].message", is("NotEmpty")));
    }

    @Test
    public void saveJavaScriptFrameworkWithInvalidVersionSyntaxTest() throws Exception {
        JavaScriptFramework framework = new JavaScriptFramework("Angular", "4.sd.1", LocalDate.of(2021, 12, 22), HypeLevel.MEDIUM);

        mockMvc.perform(post(BASE_ENDPOINT).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[0].field", is("version")))
                .andExpect(jsonPath("$.errors[0].message", is("Pattern")));
    }

    @Test
    public void updateJavaScriptFrameworkTest() throws Exception {
        JavaScriptFramework oldVersion = new JavaScriptFramework("Angular", "4.2.1", LocalDate.of(2021, 12, 22), HypeLevel.MEDIUM);
        JavaScriptFramework newVersion = new JavaScriptFramework("Angular", "4.2.2", LocalDate.of(2022, 5, 22), HypeLevel.HIGH);
        repository.save(oldVersion);

        String endpoint = BASE_ENDPOINT + "/" + oldVersion.getId();

        mockMvc.perform(put(endpoint).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(newVersion)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(newVersion.getName())))
                .andExpect(jsonPath("$.version", is(newVersion.getVersion())))
                .andExpect(jsonPath("$.deprecationDate", is(newVersion.getDeprecationDate().toString())))
                .andExpect(jsonPath("$.hypeLevel", is(newVersion.getHypeLevel().toString())));
    }

    @Test
    public void updateNotPresentJavaScriptFrameworkTest() throws Exception {
        long id = 1000L;
        String endpoint = BASE_ENDPOINT + "/" + id;
        JavaScriptFramework newVersion = new JavaScriptFramework("Angular", "4.2.2", LocalDate.of(2022, 5, 22), HypeLevel.HIGH);

        mockMvc.perform(put(endpoint).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(newVersion)))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Framework with id " + id + " was not found"));
    }

    @Test
    public void deleteJavaScriptFrameworkTest() throws Exception {
        List<JavaScriptFramework> frameworks = Arrays.asList(
                new JavaScriptFramework("Angular", "1.1.1", LocalDate.of(2021, 12, 25), HypeLevel.HIGH),
                new JavaScriptFramework("Vue.js", "1.2.1", LocalDate.of(2021, 11, 22), HypeLevel.LOW),
                new JavaScriptFramework("AngularJS", "1.3.1", LocalDate.of(2021, 9, 12), HypeLevel.MEDIUM)
        );
        repository.saveAll(frameworks);

        long deletedFrameworkId = frameworks.get(0).getId();
        String endpoint = BASE_ENDPOINT + "/" + deletedFrameworkId;

        mockMvc.perform(delete(endpoint))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteNotPresentJavaScriptFrameworkTest() throws Exception {
        long deletedFrameworkId = 1000L;
        String endpoint = BASE_ENDPOINT + "/" + deletedFrameworkId;

        mockMvc.perform(delete(endpoint))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Framework with id " + deletedFrameworkId + " was not found"));
    }

}
