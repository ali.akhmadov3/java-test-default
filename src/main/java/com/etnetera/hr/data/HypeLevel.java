package com.etnetera.hr.data;

/**
 * Enum containing hype level values.
 *
 * @author Etnetera
 */
public enum HypeLevel {
    LOW,
    MEDIUM,
    HIGH
}
