package com.etnetera.hr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;

/**
 * Simple REST controller for [JavaScriptFramework]s.
 *
 * @author Etnetera
 */
@RestController
@RequestMapping("api/v1/frameworks")
public class JavaScriptFrameworkController extends EtnRestController {

    /**
     * Repository for [JavaScriptFramework]s.
     */
    private final JavaScriptFrameworkRepository repository;

    /**
     * Constructor for [JavaScriptFrameworkController]
     *
     * @param repository injecting [JavaScriptFrameworkRepository] repository bean
     */
    @Autowired
    public JavaScriptFrameworkController(JavaScriptFrameworkRepository repository) {
        this.repository = repository;
    }

    /**
     * Get all [JavaScriptFramework]s
     *
     * @return Iterable of all found [JavaScriptFramework]s wrapped in [ResponseEntity]
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Iterable<JavaScriptFramework>> getAllFrameworks() {
        return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
    }

    /**
     * Get [JavaScriptFramework] with the given id.
     * ResponseStatusException is thrown with HTTP status 404 (NOT FOUND) if [JavaScriptFramework] does not exist.
     *
     * @param id javascript framework id
     * @return found [JavaScriptFramework] wrapped in [ResponseEntity]
     */
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<JavaScriptFramework> getFrameworkById(@PathVariable Long id) {
        return repository.findById(id)
                .map(javaScriptFramework -> new ResponseEntity<>(javaScriptFramework, HttpStatus.OK))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Framework with id " + id + " was not found"));
    }

    /**
     * Search [JavaScriptFramework]s with the alias (beginning of the name).
     *
     * @param name javascript framework name alias
     * @return Iterable of all found [JavaScriptFramework]s wrapped in [ResponseEntity]
     */
    @GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Iterable<JavaScriptFramework>> searchFrameworksByName(@RequestParam String name) {
        return new ResponseEntity<>(repository.findByNameContaining(name), HttpStatus.OK);
    }

    /**
     * Creates new [JavaScriptFramework] from the given payload.
     * ResponseStatusException is thrown with HTTP status 400 (BAD REQUEST) if the payload is not provided.
     *
     * @param javaScriptFramework request body payload for new [JavaScriptFramework]
     * @return new persisted [JavaScriptFramework] wrapped in [ResponseEntity]
     */
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<JavaScriptFramework> saveFramework(@RequestBody @Valid JavaScriptFramework javaScriptFramework) {
        if (javaScriptFramework == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Payload not provided.");
        }

        return new ResponseEntity<>(repository.save(javaScriptFramework), HttpStatus.CREATED);
    }

    /**
     * Update [JavaScriptFramework] with the given id and modify based on the given payload.
     * ResponseStatusException is thrown with HTTP status 400 (BAD REQUEST) if the payload is not provided.
     * ResponseStatusException is thrown with HTTP status 404 (NOT FOUND) if [JavaScriptFramework] does not exist.
     *
     * @param id                  javascript framework id to update
     * @param javaScriptFramework request body payload for new version of [JavaScriptFramework]
     * @return updated [JavaScriptFramework] wrapped in [ResponseEntity]
     */
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<JavaScriptFramework> updateFramework(@PathVariable Long id, @RequestBody @Valid JavaScriptFramework javaScriptFramework) {
        if (javaScriptFramework == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Payload not provided.");
        }

        JavaScriptFramework framework = repository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Framework with id " + id + " was not found"));
        framework.setName(javaScriptFramework.getName());
        framework.setVersion(javaScriptFramework.getVersion());
        framework.setDeprecationDate(javaScriptFramework.getDeprecationDate());
        framework.setHypeLevel(javaScriptFramework.getHypeLevel());

        return new ResponseEntity<>(repository.save(framework), HttpStatus.OK);
    }

    /**
     * Delete [JavaScriptFramework] with the given id.
     * ResponseStatusException is thrown with HTTP status 404 (NOT FOUND) if [JavaScriptFramework] does not exist.
     *
     * @param id javascript framework id to delete
     * @return ResponseEntity with HTTP status 204 (NO CONTENT)
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<JavaScriptFramework> deleteFramework(@PathVariable Long id) {
        JavaScriptFramework framework = repository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Framework with id " + id + " was not found"));
        repository.delete(framework);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
